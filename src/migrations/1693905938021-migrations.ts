import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrations1693905938021 implements MigrationInterface {
    name = 'Migrations1693905938021'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`shop\` (\`id\` varchar(36) NOT NULL, \`name\` text NOT NULL, \`lat\` text NOT NULL, \`lng\` text NOT NULL, \`createdDateTime\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`createdBy\` varchar(300) NOT NULL, \`lastChangedDateTime\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`lastChangedBy\` varchar(300) NOT NULL, \`deletedAt\` datetime(6) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`banner\` (\`id\` varchar(36) NOT NULL, \`name\` text NOT NULL, \`url\` text NOT NULL, \`createdDateTime\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`createdBy\` varchar(300) NOT NULL, \`lastChangedDateTime\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`lastChangedBy\` varchar(300) NOT NULL, \`deletedAt\` datetime(6) NULL, \`shopId\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`user\` (\`id\` varchar(36) NOT NULL, \`username\` varchar(255) NOT NULL, \`email\` varchar(255) NOT NULL, \`password\` varchar(255) NOT NULL, \`role\` enum ('admin', 'user') NOT NULL DEFAULT 'user', \`created_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updated_at\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), UNIQUE INDEX \`IDX_e12875dfb3b1d92d7d7c5377e2\` (\`email\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`banner\` ADD CONSTRAINT \`FK_5058718a4b67cdeb282312850f7\` FOREIGN KEY (\`shopId\`) REFERENCES \`shop\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`banner\` DROP FOREIGN KEY \`FK_5058718a4b67cdeb282312850f7\``);
        await queryRunner.query(`DROP INDEX \`IDX_e12875dfb3b1d92d7d7c5377e2\` ON \`user\``);
        await queryRunner.query(`DROP TABLE \`user\``);
        await queryRunner.query(`DROP TABLE \`banner\``);
        await queryRunner.query(`DROP TABLE \`shop\``);
    }

}
