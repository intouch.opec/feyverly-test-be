import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrations1694074744246 implements MigrationInterface {
    name = 'Migrations1694074744246'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE \`cat_toy_entity\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`catId\` int NULL, \`sizeHeight\` int NOT NULL, \`sizeWidth\` int NOT NULL, \`sizeLength\` int NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`cat\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`color\` varchar(255) NOT NULL, \`age\` int NULL, \`cutenessLevel\` text NOT NULL, \`lastVetVisit\` datetime NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`deletedAt\` datetime(6) NULL, \`homeId\` int NULL, \`sizeHeight\` int NOT NULL, \`sizeWidth\` int NOT NULL, \`sizeLength\` int NOT NULL, UNIQUE INDEX \`REL_97f120ed85f1eca4908bc4543c\` (\`homeId\`), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`cat_home_pillow_entity\` (\`id\` int NOT NULL AUTO_INCREMENT, \`color\` varchar(255) NOT NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`homeId\` int NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`cat_home_entity\` (\`id\` int NOT NULL AUTO_INCREMENT, \`name\` varchar(255) NOT NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), PRIMARY KEY (\`id\`)) ENGINE=InnoDB`);
        await queryRunner.query(`CREATE TABLE \`cat_friends_cat\` (\`catId_1\` int NOT NULL, \`catId_2\` int NOT NULL, INDEX \`IDX_ba407ac9e21d01a6ba443c27d6\` (\`catId_1\`), INDEX \`IDX_467ed799a40f2b816bf6a8f524\` (\`catId_2\`), PRIMARY KEY (\`catId_1\`, \`catId_2\`)) ENGINE=InnoDB`);
        await queryRunner.query(`ALTER TABLE \`shop\` DROP COLUMN \`createdDateTime\``);
        await queryRunner.query(`ALTER TABLE \`shop\` DROP COLUMN \`lastChangedDateTime\``);
        await queryRunner.query(`ALTER TABLE \`banner\` DROP COLUMN \`createdDateTime\``);
        await queryRunner.query(`ALTER TABLE \`banner\` DROP COLUMN \`lastChangedDateTime\``);
        await queryRunner.query(`ALTER TABLE \`shop\` ADD \`createdAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`shop\` ADD \`updatedAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`banner\` ADD \`createdAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`banner\` ADD \`updatedAt\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`cat_toy_entity\` ADD CONSTRAINT \`FK_441e45eec97723bb12d5123213a\` FOREIGN KEY (\`catId\`) REFERENCES \`cat\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`cat\` ADD CONSTRAINT \`FK_97f120ed85f1eca4908bc4543c6\` FOREIGN KEY (\`homeId\`) REFERENCES \`cat_home_entity\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`cat_home_pillow_entity\` ADD CONSTRAINT \`FK_62cf9d1690964fb54245eba4872\` FOREIGN KEY (\`homeId\`) REFERENCES \`cat_home_entity\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE \`cat_friends_cat\` ADD CONSTRAINT \`FK_ba407ac9e21d01a6ba443c27d67\` FOREIGN KEY (\`catId_1\`) REFERENCES \`cat\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE \`cat_friends_cat\` ADD CONSTRAINT \`FK_467ed799a40f2b816bf6a8f5246\` FOREIGN KEY (\`catId_2\`) REFERENCES \`cat\`(\`id\`) ON DELETE CASCADE ON UPDATE CASCADE`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`cat_friends_cat\` DROP FOREIGN KEY \`FK_467ed799a40f2b816bf6a8f5246\``);
        await queryRunner.query(`ALTER TABLE \`cat_friends_cat\` DROP FOREIGN KEY \`FK_ba407ac9e21d01a6ba443c27d67\``);
        await queryRunner.query(`ALTER TABLE \`cat_home_pillow_entity\` DROP FOREIGN KEY \`FK_62cf9d1690964fb54245eba4872\``);
        await queryRunner.query(`ALTER TABLE \`cat\` DROP FOREIGN KEY \`FK_97f120ed85f1eca4908bc4543c6\``);
        await queryRunner.query(`ALTER TABLE \`cat_toy_entity\` DROP FOREIGN KEY \`FK_441e45eec97723bb12d5123213a\``);
        await queryRunner.query(`ALTER TABLE \`banner\` DROP COLUMN \`updatedAt\``);
        await queryRunner.query(`ALTER TABLE \`banner\` DROP COLUMN \`createdAt\``);
        await queryRunner.query(`ALTER TABLE \`shop\` DROP COLUMN \`updatedAt\``);
        await queryRunner.query(`ALTER TABLE \`shop\` DROP COLUMN \`createdAt\``);
        await queryRunner.query(`ALTER TABLE \`banner\` ADD \`lastChangedDateTime\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`banner\` ADD \`createdDateTime\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`shop\` ADD \`lastChangedDateTime\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`ALTER TABLE \`shop\` ADD \`createdDateTime\` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)`);
        await queryRunner.query(`DROP INDEX \`IDX_467ed799a40f2b816bf6a8f524\` ON \`cat_friends_cat\``);
        await queryRunner.query(`DROP INDEX \`IDX_ba407ac9e21d01a6ba443c27d6\` ON \`cat_friends_cat\``);
        await queryRunner.query(`DROP TABLE \`cat_friends_cat\``);
        await queryRunner.query(`DROP TABLE \`cat_home_entity\``);
        await queryRunner.query(`DROP TABLE \`cat_home_pillow_entity\``);
        await queryRunner.query(`DROP INDEX \`REL_97f120ed85f1eca4908bc4543c\` ON \`cat\``);
        await queryRunner.query(`DROP TABLE \`cat\``);
        await queryRunner.query(`DROP TABLE \`cat_toy_entity\``);
    }

}
