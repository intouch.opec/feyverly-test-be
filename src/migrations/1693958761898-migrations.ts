import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrations1693958761898 implements MigrationInterface {
    name = 'Migrations1693958761898'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`banner\` DROP COLUMN \`lastChangedBy\``);
        await queryRunner.query(`ALTER TABLE \`shop\` DROP COLUMN \`lastChangedBy\``);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`shop\` ADD \`lastChangedBy\` varchar(300) NOT NULL`);
        await queryRunner.query(`ALTER TABLE \`banner\` ADD \`lastChangedBy\` varchar(300) NOT NULL`);
    }

}
