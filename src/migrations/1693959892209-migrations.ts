import { MigrationInterface, QueryRunner } from "typeorm";

export class Migrations1693959892209 implements MigrationInterface {
    name = 'Migrations1693959892209'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`banner\` CHANGE \`url\` \`url\` text NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`banner\` CHANGE \`url\` \`url\` text NOT NULL`);
    }

}
