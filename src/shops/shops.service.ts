import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateShopDto } from './dto/create-shop.dto';
import { UpdateShopDto } from './dto/update-shop.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import { Shop } from './entities/shop.entity';
import { SearchShopDto } from './dto/search-shop.dto';
import { PageDto } from '../utils/pagination/page.dto';
import { PageMetaDto } from '../utils/pagination/page-meta.dto';

@Injectable()
export class ShopService {
  constructor(
    @InjectRepository(Shop)
    private shopRepository: Repository<Shop>,
  ) {}

  create(createShopDto: CreateShopDto, userId: string) {
    const shop = new Shop();
    shop.name = createShopDto.name;
    shop.lat = createShopDto.lat;
    shop.lng = createShopDto.lng;
    shop.createdBy = userId;
    return this.shopRepository.save(shop);
  }

  async findAll(search: SearchShopDto): Promise<PageDto<Shop, SearchShopDto>> {
    const skip = search.take * (search.page - 1);
    const [entities, itemCount] = await this.shopRepository.findAndCount({
      where: {
        name: search.name ? Like(`%${search.name}%`) : undefined,
      },
      skip,
      take: search.take,
    });
    const pageMetaDto = new PageMetaDto<SearchShopDto>({
      itemCount,
      pageOptionsDto: search,
    });
    return new PageDto(entities, pageMetaDto);
  }

  async findOne(id: string) {
    const shop = await this.shopRepository.findOne({
      where: { id },
    });
    if (!shop) {
      throw new NotFoundException(`The banner id '${id}' not found.`);
    }
    return shop;
  }

  async update(id: string, updateShopDto: UpdateShopDto) {
    const shop = new Shop();
    shop.id = id;
    shop.name = updateShopDto.name;
    shop.lat = updateShopDto.lat;
    shop.lng = updateShopDto.lng;
    this.shopRepository.save(shop);
    return await this.findOne(id);
  }

  async remove(id: string) {
    const shop = await this.findOne(id);
    return this.shopRepository.remove(shop);
  }
}
