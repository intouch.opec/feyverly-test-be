import { ApiProperty } from '@nestjs/swagger';
import { PageOptionsDto } from '../../utils/pagination/page-options.dto';

export class SearchShopDto extends PageOptionsDto {
  @ApiProperty({ required: false })
  name?: string;
}
