import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  Request,
  Query,
} from '@nestjs/common';
import { ShopService } from './shops.service';
import { CreateShopDto } from './dto/create-shop.dto';
import { UpdateShopDto } from './dto/update-shop.dto';
import {
  ApiTags,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiCreatedResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { SearchShopDto } from './dto/search-shop.dto';

@ApiTags('Shop')
@UseGuards(JwtAuthGuard)
@ApiBearerAuth('access-token')
@Controller('shops')
export class ShopController {
  constructor(private readonly shopService: ShopService) {}

  @Post()
  @ApiCreatedResponse()
  create(@Body() createShopDto: CreateShopDto, @Request() req) {
    return this.shopService.create(createShopDto, req?.user?.userId);
  }

  @Get()
  @ApiOkResponse({ description: 'The resource was returned successfully' })
  findAll(@Query() pageOptionsDto: SearchShopDto) {
    return this.shopService.findAll(pageOptionsDto);
  }

  @Get(':id')
  @ApiNotFoundResponse({ description: 'Resource not found' })
  @ApiOkResponse({ description: 'The resource was returned successfully' })
  findOne(@Param('id') id: string) {
    return this.shopService.findOne(id);
  }

  @Put(':id')
  @ApiNotFoundResponse({ description: 'Resource not found' })
  @ApiOkResponse({ description: 'The resource was returned successfully' })
  update(@Param('id') id: string, @Body() updateShopDto: UpdateShopDto) {
    return this.shopService.update(id, updateShopDto);
  }

  @Delete(':id')
  @ApiNotFoundResponse({ description: 'Resource not found' })
  @ApiOkResponse({ description: 'The resource was returned successfully' })
  remove(@Param('id') id: string) {
    return this.shopService.remove(id);
  }
}
