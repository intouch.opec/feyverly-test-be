import { ApiProperty } from '@nestjs/swagger';
import { PageOptionsDto } from '../../utils/pagination/page-options.dto';

export class SearchUserDto extends PageOptionsDto {
  @ApiProperty({ required: false })
  username?: string | null;

  @ApiProperty({ required: false })
  email?: string | null;
}
