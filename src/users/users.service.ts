import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Like, Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';
import * as bcrypt from 'bcrypt';
import { UpdateUserDto } from './dto/update-user.dto';
import { isEmpty } from 'class-validator';
import { PageDto } from '../utils/pagination/page.dto';
import { SearchUserDto } from './dto/search-user.dto';
import { PageMetaDto } from '../utils/pagination/page-meta.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private userRepository: Repository<User>,
  ) {}

  async create(createUserDto: CreateUserDto) {
    try {
      const user = new User();
      const hashPassword = await bcrypt.hash(createUserDto.password, 10);
      user.username = createUserDto.username;
      user.email = createUserDto.email;
      user.password = hashPassword;
      user.role = createUserDto.role;
      return this.userRepository.save(user);
    } catch (err) {
      throw new InternalServerErrorException(
        `Error creating ${err} user ${err.message}`,
      );
    }
  }

  async findOne(email: string, password: string): Promise<User | undefined> {
    try {
      const user = await this.userRepository.findOne({
        where: { email },
      });
      if (isEmpty(user)) {
        throw new NotFoundException(`The user email is '${email}' not found.`);
      }
      const isMatch = await bcrypt.compare(password, user.password);
      if (isMatch) {
        return user;
      } else {
        throw new BadRequestException(`User not found`);
      }
    } catch (err) {
      throw new InternalServerErrorException(
        `Error finding ${err} user ${err.message}`,
      );
    }
  }

  async findAll(search: SearchUserDto): Promise<PageDto<User, SearchUserDto>> {
    const skip = search.take * (search.page - 1);
    const [entities, itemCount] = await this.userRepository.findAndCount({
      where: {
        username: search.username ? Like(`%${search.username}%`) : undefined,
        email: search.email ? Like(`%${search.email}%`) : undefined,
      },
      skip,
      take: search.take,
    });
    const pageMetaDto = new PageMetaDto<SearchUserDto>({
      itemCount,
      pageOptionsDto: search,
    });
    return new PageDto(entities, pageMetaDto);
  }

  async findOneById(id: string): Promise<User> {
    return await this.userRepository.findOne({ where: { id } });
  }

  async update(id: string, updateBannerDto: UpdateUserDto): Promise<User> {
    const user = await this.findOneById(id);
    user.role = updateBannerDto.role;
    return this.userRepository.save(user);
  }

  async remove(id: string) {
    const user = await this.findOneById(id);
    return this.userRepository.remove(user);
  }
}
