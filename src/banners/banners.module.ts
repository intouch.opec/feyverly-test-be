import { Module } from '@nestjs/common';
import { BannerService } from './banners.service';
import { BannerController } from './banners.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Banner } from './entities/banner.entity';
import { ShopModule } from '../shops/shops.module';

@Module({
  imports: [TypeOrmModule.forFeature([Banner]), ShopModule],
  controllers: [BannerController],
  providers: [BannerService],
})
export class BannerModule {}
