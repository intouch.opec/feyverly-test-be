import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  UseGuards,
  Request,
  Query,
} from '@nestjs/common';
import { BannerService } from './banners.service';
import { CreateBannerDto } from './dto/create-banner.dto';
import { UpdateBannerDto } from './dto/update-banner.dto';
import {
  ApiTags,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiCreatedResponse,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { SearchBannderDto } from './dto/search-banner.dto';

@ApiTags('Banner')
@Controller('banners')
@UseGuards(JwtAuthGuard)
export class BannerController {
  constructor(private readonly bannerService: BannerService) {}

  @Post()
  @ApiCreatedResponse()
  create(@Body() createBannerDto: CreateBannerDto, @Request() req) {
    return this.bannerService.create(createBannerDto, req.user.userId);
  }

  @Get()
  @ApiOkResponse({ description: 'The resource was returned successfully' })
  findAll(@Query() pageOptionsDto: SearchBannderDto) {
    return this.bannerService.findAll(pageOptionsDto);
  }

  @Get(':id')
  @ApiNotFoundResponse({ description: 'Resource not found' })
  @ApiOkResponse({ description: 'The resource was returned successfully' })
  findOne(@Param('id') id: string) {
    return this.bannerService.findOne(id);
  }

  @Put(':id')
  @ApiNotFoundResponse({ description: 'Resource not found' })
  @ApiOkResponse({ description: 'The resource was returned successfully' })
  update(@Param('id') id: string, @Body() updateBannerDto: UpdateBannerDto) {
    return this.bannerService.update(id, updateBannerDto);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @ApiNotFoundResponse({ description: 'Resource not found' })
  remove(@Param('id') id: string) {
    return this.bannerService.remove(id);
  }

  @Delete(':id/url')
  @UseGuards(JwtAuthGuard)
  @ApiNotFoundResponse({ description: 'Resource not found' })
  removeImage(@Param('id') id: string) {
    return this.bannerService.removeUrl(id);
  }
}
