import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBannerDto } from './dto/create-banner.dto';
import { UpdateBannerDto } from './dto/update-banner.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Banner } from './entities/banner.entity';
import { Like, Repository } from 'typeorm';
import { ShopService } from '../shops/shops.service';
import { SearchBannderDto } from './dto/search-banner.dto';
import { PageDto } from '../utils/pagination/page.dto';
import { PageMetaDto } from '../utils/pagination/page-meta.dto';

@Injectable()
export class BannerService {
  constructor(
    @InjectRepository(Banner)
    private bannerRepository: Repository<Banner>,
    private shopService: ShopService,
  ) {}

  async create(createBannerDto: CreateBannerDto, userId: string) {
    const bannder = new Banner();
    bannder.name = createBannerDto.name;
    bannder.url = createBannerDto.url;
    bannder.createdBy = userId;
    bannder.shop = await this.shopService.findOne(createBannerDto.shopId);
    return this.bannerRepository.save(bannder);
  }

  async findAll(
    search: SearchBannderDto,
  ): Promise<PageDto<Banner, SearchBannderDto>> {
    const skip = search.take * (search.page - 1);
    const [entities, itemCount] = await this.bannerRepository.findAndCount({
      where: {
        name: search.name ? Like(`%${search.name}%`) : undefined,
      },
      skip,
      take: search.take,
    });
    const pageMetaDto = new PageMetaDto<SearchBannderDto>({
      itemCount,
      pageOptionsDto: search,
    });
    return new PageDto(entities, pageMetaDto);
  }

  async findOne(id: string) {
    const banner = await this.bannerRepository.findOne({ where: { id } });
    if (!banner) {
      throw new NotFoundException(`The banner id '${id}' not found.`);
    }
    return banner;
  }

  async update(id: string, updateBannerDto: UpdateBannerDto) {
    const bannder = new Banner();
    bannder.id = id;
    bannder.name = updateBannerDto.name;
    bannder.url = updateBannerDto.url;
    bannder.shop = await this.shopService.findOne(updateBannerDto.shopId);
    return this.bannerRepository.save(bannder);
  }

  async remove(id: string) {
    const bannder = await this.findOne(id);
    return this.bannerRepository.remove(bannder);
  }

  async removeUrl(id: string) {
    const bannder = await this.findOne(id);
    bannder.url = '';
    return this.bannerRepository.save(bannder);
  }
}
