import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import typeorm from './database/config.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ShopModule } from './shops/shops.module';
import { BannerModule } from './banners/banners.module';
import { UploadFileController } from './upload-file/upload-file.controller';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..'),
    }),
    ConfigModule.forRoot({
      isGlobal: true,
      load: [typeorm],
    }),
    TypeOrmModule.forRootAsync({
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) =>
        configService.get('typeorm'),
    }),
    AuthModule,
    UsersModule,
    ShopModule,
    BannerModule,
  ],
  controllers: [AppController, UploadFileController],
  providers: [AppService],
})
export class AppModule {}
