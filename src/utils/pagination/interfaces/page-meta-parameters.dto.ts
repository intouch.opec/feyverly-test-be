import { PageOptionsDto } from '../page-options.dto';

export interface PageMetaDtoParameters<T extends PageOptionsDto> {
  pageOptionsDto: T;
  itemCount: number;
}
